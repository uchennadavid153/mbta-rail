import { Pipe, PipeTransform } from '@angular/core';
import { Observable } from 'rxjs';

@Pipe({
  name: 'filter'
})

export class FilterPipe implements PipeTransform {

  transform(value: any[], args: any): any | null {
    let data = value.filter((data) => data.attributes.long_name.trim().toLowerCase().toString().includes(args))
   if(data.length === 0) return value
    return data    
  }

  

}

@Pipe({
  name:"filterStop"
})
export class FilterStopPipe implements PipeTransform {
  transform(value: any[], args: any): any | null {
    let data = value.filter((data) => data.attributes.name.trim().toLowerCase().toString().includes(args))
   if(data.length === 0) return value
    return data    
  }

}


@Pipe({
  name:"filterDirection"
})
export class FilterDirectionpPipe implements PipeTransform {
  transform(value: any[], args: any): any | null {
    let data = value.filter((data) => data.attributes.direction_names[0].trim().toLowerCase().toString().includes(args))
   if(data.length === 0) return value
    return data    
  }
}



@Pipe({
  name:"filterPrediction"
})
export class FilterPredictionpPipe implements PipeTransform {
  transform(value: any[], args: any): any | null {
    let data = value.filter((data) => data.attributes.direction_names[0].trim().toLowerCase().toString().includes(args))
   if(data.length === 0) return value
    return data    
  }
}