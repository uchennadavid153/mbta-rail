import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { CrudService } from 'src/app/services/crud.service';

@Component({
  selector: 'app-direction',
  templateUrl: './direction.component.html',
  styleUrls: ['./direction.component.scss']
})
export class DirectionComponent implements OnInit {
  formValue=""
  directionData$: any
  name: string = ''
  modalBoolean:boolean = false
  
  constructor(private route: ActivatedRoute, private crudService: CrudService, private router:Router) { }

  ngOnInit(): void {
    this.route.params.subscribe((id: any) => {
      this.crudService.directionController(id).subscribe()
    })
    this.crudService.directionSubject$.subscribe((data) => {
      this.directionData$ = data
    })
  }

  showModal(direction_name:string, name:string){
    this.modalBoolean = true
    this.name = direction_name +'$'+name
  }
  predictionNavigator(){
    let id = this.name.split('$')[0]
    let name = this.name.split('$')[1]
    this.router.navigate(['/crud/prediction/'+id+'/direction/'+name])       
  }
}
