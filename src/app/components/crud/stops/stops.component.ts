import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Route, Router } from '@angular/router';
import { CrudService } from 'src/app/services/crud.service';

@Component({
  selector: 'app-stops',
  templateUrl: './stops.component.html',
  styleUrls: ['./stops.component.scss']
})
export class StopsComponent implements OnInit {
  stopData$: any
  formValue=''
  name:string = ''
  modalBoolean:boolean = false; stopId:string = ""
  constructor(private route: ActivatedRoute, private crudService: CrudService, private router:Router) { }

  ngOnInit(): void {
    this.route.params.subscribe((id: any) => {
      this.crudService.stopsController(parseFloat(id.id)).subscribe()
    })
    this.crudService.stopsSubject$.subscribe(data => {
      this.stopData$ = data    
    })

  }
  showModal(stopId:string, name:string){
    this.stopId = stopId; this.name = name
    this.modalBoolean = true
  }
  predictionStop(){
    this.router.navigate(['/crud/prediction/'+this.stopId+'/stop/'+this.name])
  }

}
