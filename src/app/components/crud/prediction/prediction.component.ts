import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { CrudService } from 'src/app/services/crud.service';

@Component({
  selector: 'app-prediction',
  templateUrl: './prediction.component.html',
  styleUrls: ['./prediction.component.scss']
})
export class PredictionComponent implements OnInit {
  predictionData$: any
  name: string = ''
  constructor(private route: ActivatedRoute, private crudService: CrudService) { }



  ngOnInit(): void {
    this.route.params.subscribe((data)=> {
      this.name = data.name
    this.crudService.predictionsController(data.id, data.type).subscribe()
    })
   this.crudService.subject$.subscribe(data => {
     this.predictionData$ = data     
   })
  }

}
