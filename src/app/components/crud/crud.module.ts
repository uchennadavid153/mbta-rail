import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CrudRoutingModule } from './crud-routing.module';
import { CrudComponent } from './crud.component';
import { LoadingComponent } from '../loading/loading.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { FilterDirectionpPipe, FilterPipe, FilterStopPipe } from 'src/app/pipe/filter.pipe';
import { StopsComponent } from './stops/stops.component';
import { PredictionComponent } from './prediction/prediction.component';
import { DirectionComponent } from './direction/direction.component';
import { DefaultComponent } from './default/default.component';


@NgModule({
  declarations: [
    CrudComponent,
    LoadingComponent,
    FilterPipe,
    StopsComponent,
    PredictionComponent,
    DirectionComponent,
    DefaultComponent,
    FilterStopPipe,
    FilterDirectionpPipe
  ],
  imports: [
    CommonModule,
    CrudRoutingModule,
    FormsModule,
    ReactiveFormsModule,
  ]
})
export class CrudModule { }
