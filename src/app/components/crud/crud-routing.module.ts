import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { DefaultComponent } from './default/default.component';
import { CrudComponent } from './crud.component';
import { DirectionComponent } from './direction/direction.component';
import { PredictionComponent } from './prediction/prediction.component';
import { StopsComponent } from './stops/stops.component';

const routes: Routes = [{ path: '', component: CrudComponent , children:[
  {
    path:'stops/:id',component:StopsComponent
  },
  {
    path:"direction/:id", component:DirectionComponent
  },  {
    path:'prediction/:id/:type/:name',component:PredictionComponent
  },
  {
    path:"", component:DefaultComponent
  }
]}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class CrudRoutingModule {}
