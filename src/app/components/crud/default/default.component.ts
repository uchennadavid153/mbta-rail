import { Component, OnInit } from '@angular/core';
import { CrudService } from 'src/app/services/crud.service';

@Component({
  selector: 'app-default',
  templateUrl: './default.component.html',
  styleUrls: ['./default.component.scss']
})
export class DefaultComponent implements OnInit {

  formValue = ''
  routeData$: any = []
  modalBoolean:boolean = false; destinationId:string = ''; stopId:string = ""
  constructor(public crudService: CrudService) { }

  ngOnInit(): void {
    this.crudService.routesController().subscribe() 
    this.crudService.routesSubject$.subscribe(data => {
      this.routeData$ = data
    })
  }
  showModal(stopId:string, destinationId:string){
    this.destinationId = destinationId; 
    this.stopId = stopId
    this.modalBoolean = true
  }


}
