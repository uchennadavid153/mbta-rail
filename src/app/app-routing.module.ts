import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { DefaultComponent } from './components/default/default.component';

const routes: Routes = [
  {
    path:"", component:DefaultComponent
  },
  { path: 'crud', loadChildren: () => import('./components/crud/crud.module').then(m => m.CrudModule) },
  {path:"**", redirectTo:""}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
