import { Injectable } from '@angular/core';
import {
    HttpRequest,
    HttpHandler,
    HttpEvent,
    HttpInterceptor,
    HttpHeaders
} from '@angular/common/http';
import { Observable } from 'rxjs';


@Injectable()
export class HttpInterceptors implements HttpInterceptor {
    headers = {
        'Content-Type': 'application/json',
        "Access-Control-Allow-Origin": '*',
        "Access-Control-Allow-Headers":
            "Origin, X-Requested-With,Content-Type, Accept, Authorization",
    }

    intercept(request: HttpRequest<unknown>, next: HttpHandler): Observable<HttpEvent<unknown>> {

        const req = request.clone({
            // headers: new HttpHeaders(this.headers)
        })
        return next.handle(req);
    }
}
