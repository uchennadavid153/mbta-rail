import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { ErrorHandlerService } from './error.service';
import { catchError, finalize, map, tap } from 'rxjs/operators';
import { environment } from 'src/environments/environment';
import { BehaviorSubject, Observable, throwError } from 'rxjs';
import { LoadingService } from './loading.service';

@Injectable({
  providedIn: 'root'
})
export class CrudService {
  url: string = environment.url
  
  private subject = new BehaviorSubject<any[]>([])
  private routesSubject = new BehaviorSubject<any[]>([])
  private stopsSubject = new BehaviorSubject<any[]>([])
  private directionsubject = new BehaviorSubject<any[]>([])
  subject$:Observable<any[]> = this.subject.asObservable()
  stopsSubject$:Observable<any[]> = this.stopsSubject.asObservable()
  routesSubject$:Observable<any[]> = this.routesSubject.asObservable()
  directionSubject$:Observable<any[]> = this.directionsubject.asObservable()
  constructor(private http: HttpClient, private errorHandler: ErrorHandlerService, private loadingService:LoadingService) {  
  }

  

 private getDataController(params:string){

  this.loadingService.loadingOn()
  return this.http.get(environment.url+params).pipe(
    map((res:any) => res['data'] ),
    catchError((err: any) => {
      return throwError(this.errorHandler.handleError(err));
    }),
    finalize(()=> this.loadingService.loadingOff()),
  )
 }

  stopsController(filter:number): Observable<any>{
    return this.getDataController(`stops?filter[route_type]=${filter}`).pipe(
      tap((data:any[]) => this.stopsSubject.next(data)
      ),
    ) 
  }

  routesController():Observable<any>{
    return this.getDataController('routes?filter[type]=0,1').pipe(
      tap((data:any[]) => this.routesSubject.next(data)
       ),
    ) 
  }
  directionController(name:string):Observable<any>{
    return this.getDataController(`routes?filter[direction_id]=${name}`).pipe(
      tap((data:any[]) => this.directionsubject.next(data)
       ),
    ) 
  }

  predictionsController(id:string, type:"direction"|"stop"):Observable<any>{
    if(type === "direction"){
      return this.getDataController('predictions?filter[direction_id]='+id).pipe(
        tap((data:any[]) => this.subject.next(data))
      )
    }else{
      return this.getDataController(`predictions?filter[stop]=${id}`).pipe(
        tap((data:any[]) => this.subject.next(data))
        )
      }
    }

}
