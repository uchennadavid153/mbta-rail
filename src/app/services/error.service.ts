import { HttpErrorResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
    providedIn: 'root'
})
export class ErrorHandlerService {

    constructor() { }
    handleError(error: HttpErrorResponse): Error {
        if (error.status === 0) {

            console.error('An error occurred:', error.error);
        } else {

            console.error(
                `Backend returned code ${error.status}, body was: `, error.error);
        }
        return new Error('Something bad happened; please try again later.')
    }
}
