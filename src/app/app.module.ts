import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { DefaultComponent } from './components/default/default.component';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http'
import { HttpInterceptors } from './interceptors/http.interceptor';


@NgModule({
  declarations: [
    AppComponent,
    DefaultComponent,

  
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule
  ],
  providers: [{

    provide:HTTP_INTERCEPTORS, useClass:HttpInterceptors, multi:true

  }],
  bootstrap: [AppComponent]
})
export class AppModule { }
